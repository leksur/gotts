package gotts

import (
	"gitlab.com/leksur/gotts/handlers"
	"gitlab.com/leksur/gotts/voices"

	"testing"
)

func TestSpeech_Speak(t *testing.T) {
	speech := Speech{Folder: "audio", Language: voices.English}
	speech.Speak("English test")
}

func TestSpeech_Speak_MPlayer_Handler(t *testing.T) {
	speech := Speech{Folder: "audio", Language: voices.English, Handler: &handlers.GoMP3{}}
	speech.Speak("Handler test")
}

func TestSpeech_Speak_voice_UkEnglish(t *testing.T) {
	speech := Speech{Folder: "audio", Language: voices.EnglishUK}
	speech.Speak("English UK test")
}

func TestSpeech_Speak_voice_Japanese(t *testing.T) {
	speech := Speech{Folder: "audio", Language: voices.Japanese}
	speech.Speak("Japanese test")
}

func TestSpeech_(t *testing.T) {
	speech := Speech{Folder: "audio", Language: voices.English}
	f, err := speech.CreateSpeechFile("Play Speech File Test", "testplay")
	if err != nil {
		t.Fatalf("CreateSpeechFile fail %v", err)
	}
	speech.PlaySpeechFile(f)
}
